package app.service;

import app.models.TransactionModel;
import com.mk.CashModel;
import com.mk.Transaction;
import com.mk.TransactionType;
import com.mk.TransferModel;

public class TransactionBuilder {

    private Transaction transaction;

    public TransactionBuilder() {
        transaction = new Transaction();
    }

    public TransactionBuilder transferModel(TransferModel transferModel) {
        this.transaction.setTitle(transferModel.getCashModel().getTitle());
        this.transaction.setDestinationAccount(transferModel.getDestinationAccount());
        this.transaction.setDestinationName(transferModel.getDestinationName());
        this.transaction.setSourceName(transferModel.getCashModel().getSourceName());
        this.transaction.setSourceAccount(transferModel.getCashModel().getSourceAccount());
        this.transaction.setAmount(transferModel.getCashModel().getAmount());
        return this;
    }

    public TransactionBuilder destinationAccount(String destinationAccount) {
        this.transaction.setDestinationAccount(destinationAccount);
        return this;
    }

    public TransactionBuilder transactionType(TransactionType type) {
        this.transaction.setTransactionType(type);
        return this;
    }

    public TransactionBuilder balance(int balance) {
        this.transaction.setBalance(balance);
        return this;
    }

    public TransactionBuilder cashModel(CashModel cashModel) {
        TransferModel transferModel = new TransferModel();
        transferModel.setCashModel(cashModel);
        return transferModel(transferModel);
    }

    public Transaction build() {
        return transaction;
    }
}
