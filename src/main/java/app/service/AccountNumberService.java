package app.service;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import app.models.AccountNumber;

import java.math.BigInteger;
import java.util.Random;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Service
public class AccountNumberService {
    @Autowired
    private MongoOperations mongo;
    private static int MIN_CODE_POINT = 48;
    private static int MAX_CODE_POINT = 58;

    public String getNewAccountNumber() {
        AccountNumber generatedRandomNumber = generateAccountNumber();

        while (mongo.exists(Query.query(where("accountNumber").is(generatedRandomNumber.getAccountNumber())), AccountNumber.class)) {
            generatedRandomNumber = generateAccountNumber();
        }

        mongo.save(generatedRandomNumber);
        return generatedRandomNumber.getAccountNumber();
    }

    private AccountNumber generateAccountNumber() {
        AccountNumber accountNumber = new AccountNumber();
        Random random = new Random();

        String nrb = accountNumber.getChecksum() + accountNumber.getBankId() + "000000000000" + random
                .ints(4, 0, 10)
                .mapToObj(String::valueOf)
                .reduce((a,b) -> a + b).get();

        int checksum = calculateChecksum(accountNumber.getCountryCode() + nrb);

        String stringifiedChecksum = String.format("%02d", checksum);
        accountNumber.setChecksum(stringifiedChecksum);

        accountNumber.setAccountNumber(stringifiedChecksum + nrb.substring(2));
        return accountNumber;
    }

    private int calculateChecksum(String accountNumber) {
        String countryCode = accountNumber.substring(0, 2);
        String code = countryCode.codePoints()
                    .map(codePoint -> codePoint - 55)
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining());
        String modifiedAccounNumber = accountNumber.substring(4) + code + accountNumber.substring(2,4);
        return 98 - modifiedAccounNumber.codePoints()
                .map(codePoint -> codePoint - 48)
                .mapToObj(BigInteger::valueOf)
                .reduce((acc, value) -> acc.multiply(BigInteger.TEN).add(value))
                .get()
                .mod(BigInteger.valueOf(97))
                .intValue();
    }

    public boolean isValid(String accountNumber) {
        String fullAccounNumber;
        if (accountNumber.matches("^PL[0-9]{26}$")) {
            fullAccounNumber = accountNumber;
        } else if (accountNumber.matches("^[0-9]{26}$")) {
            fullAccounNumber = "PL" + accountNumber;
        } else {
            return false;
        }
        return calculateChecksum(fullAccounNumber) == 97;
    }
}
