package app.service;

import com.mk.Account;
import com.mk.Client;

import javax.validation.constraints.NotNull;

public class AccountFactory {
    public static Account create(@NotNull Client client, @NotNull AccountNumberService accountNumberService) {
        assert client.getId() != null;
        Account account = new Account();
        account.setBalance(0);
        account.setOwnerId(client.getId());
        account.setAccountNumber(accountNumberService.getNewAccountNumber());
        return account;
    }
}