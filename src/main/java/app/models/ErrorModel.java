package app.models;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ErrorModel {
    private String error;
    private String field;

    public ErrorModel() {
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "[error: " + error + " <-> " + "field: " + field + "]";
    }
}
