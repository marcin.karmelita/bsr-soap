package app.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "accountNumbers")
public class AccountNumber {

    @Transient
    private String bankId = "00116360";

    @Transient
    private String countryCode = "PL";
    @Transient
    private String checksum = "00";
    @Id
    private String accountNumber;

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getChecksum() {
        return checksum;
    }

    public String getBankId() {
        return bankId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCountryCode() {
        return this.countryCode;
    }
}