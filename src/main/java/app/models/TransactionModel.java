package app.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.mk.Transaction;
import com.mk.TransferModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "transaction")
@XmlAccessorType(XmlAccessType.FIELD)
public class TransactionModel {

    @XmlElement(name = "source_account")
    @JsonProperty("source_account")
    private String sourceAccount;
    @XmlElement(name = "source_name")
    @JsonProperty("source_name")
    private String sourceName;
    private String title;
    @XmlElement(name = "destination_name")
    @JsonProperty("destination_name")
    private String destinationName;
    private Integer amount;

    public TransactionModel() {
    }

    public String getSourceAccount() {
        return sourceAccount;
    }

    public void setSourceAccount(String sourceAccount) {
        this.sourceAccount = sourceAccount;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDestinationName() {
        return destinationName;
    }

    public void setDestinationName(String destinationName) {
        this.destinationName = destinationName;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public static TransactionModel create(TransferModel transferModel) {
        TransactionModel transactionModel = new TransactionModel();
        transactionModel.setAmount(transferModel.getCashModel().getAmount());
        transactionModel.setSourceName(transferModel.getCashModel().getSourceName());
        transactionModel.setSourceAccount(transferModel.getCashModel().getSourceAccount());
        transactionModel.setTitle(transferModel.getCashModel().getTitle());
        transactionModel.setDestinationName(transferModel.getDestinationName());
        return transactionModel;
    }
}
