package app.faults;

import com.mk.Fault;

import java.util.ArrayList;
import java.util.List;

public class FaultException extends RuntimeException {

    private List<Fault> faults;

    public FaultException(String message, Fault fault) {
        super(message);
        this.faults = new ArrayList<>();
        this.faults.add(fault);
    }

    public FaultException(String message, List<Fault> faults) {
        super(message);
        this.faults = faults;
    }

    public void addFault(Fault fault) {
        if (this.faults == null) {
            this.faults = new ArrayList<>();
        }
        this.faults.add(fault);
    }

    public List<Fault> getFaults() {
        return faults;
    }

    public void setFaults(List<Fault> faults) {
        this.faults = faults;
    }
}