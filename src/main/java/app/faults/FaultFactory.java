package app.faults;

import com.mk.Fault;
import org.springframework.ws.soap.SoapFaultException;
import app.models.ErrorModel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FaultFactory {
    public enum FaultReason {
        NOT_ENOUGH_MONEY, SOURCE_ACCOUNT_NOT_EXISTS, INCORRECT_VALUE, USER_DOES_NOT_EXIST, USER_ALREADY_EXISTS
    }

    public static SoapFaultException createException(FaultReason reason) {
        String message = createExceptionMessage(reason);
        return new SoapFaultException(message);
    }
    public static SoapFaultException createException(List<ErrorModel> errorModels) {
        return new SoapFaultException(errorModels.toString());
    }

    public static FaultException createFaultException(List<FaultReason> reasons) {
        return new FaultException("Error", reasons.stream().map(FaultFactory::createFault).collect(Collectors.toList()));
    }

    public static FaultException createFaultException(FaultReason reason) {
        return new FaultException("Error", FaultFactory.createFault(reason));
    }

    private static String createExceptionMessage(FaultReason reason) {
        switch (reason) {
            case NOT_ENOUGH_MONEY:
                return "Not enough money";
            case SOURCE_ACCOUNT_NOT_EXISTS:
                return "Source account does not exist";
            case INCORRECT_VALUE:
                return "Amount must be positive";
            case USER_DOES_NOT_EXIST:
                return "User does not exist";
            default:
                return "Unknown error occurred";
        }
    }

    private static Fault createFault(FaultReason reason) {
        switch (reason) {
            case NOT_ENOUGH_MONEY:
                return new Fault("", "Not enough money");
            case SOURCE_ACCOUNT_NOT_EXISTS:
                return new Fault("", "Source account does not exist");
            case INCORRECT_VALUE:
                return new Fault("", "Amount must be positive");
            case USER_DOES_NOT_EXIST:
                return new Fault("", "User does not exist");
            case USER_ALREADY_EXISTS:
                return new Fault("", "User already exists");
            default:
                return new Fault("", "Unknown error occurred");
        }
    }


}
