package app.faults;

import com.mk.Fault;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.SoapFaultDetail;
import org.springframework.ws.soap.server.endpoint.SoapFaultMappingExceptionResolver;

import javax.xml.namespace.QName;
import java.util.List;

public class DetailSoapFaultDefinitionExceptionResolver extends SoapFaultMappingExceptionResolver {

    private static final QName FIELD = new QName("field");
    private static final QName MESSAGE = new QName("message");

    @Override
    protected void customizeFault(Object endpoint, Exception ex, SoapFault fault) {
        logger.warn("Exception processed ", ex);
        if (ex instanceof FaultException) {
            List<Fault> serviceFault = ((FaultException) ex).getFaults();
            SoapFaultDetail detail = fault.addFaultDetail();
            serviceFault.forEach(fault1 -> {
                detail.addFaultDetailElement(FIELD).addText(fault1.getField());
                detail.addFaultDetailElement(MESSAGE).addText(fault1.getMessage());
            });
        }
    }

}