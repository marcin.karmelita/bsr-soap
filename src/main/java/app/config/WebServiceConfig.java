package app.config;

import app.endpoints.Constants;
import app.faults.DetailSoapFaultDefinitionExceptionResolver;
import app.faults.FaultException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;


import org.springframework.ws.soap.server.endpoint.SoapFaultDefinition;
import org.springframework.ws.soap.server.endpoint.SoapFaultMappingExceptionResolver;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import java.io.IOException;
import java.util.Properties;


@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {

    @Bean
    public SoapFaultMappingExceptionResolver exceptionResolver(){
        SoapFaultMappingExceptionResolver exceptionResolver = new DetailSoapFaultDefinitionExceptionResolver();

        SoapFaultDefinition faultDefinition = new SoapFaultDefinition();
        faultDefinition.setFaultCode(SoapFaultDefinition.SERVER);
        exceptionResolver.setDefaultFault(faultDefinition);

        Properties errorMappings = new Properties();
        errorMappings.setProperty(Exception.class.getName(), SoapFaultDefinition.SERVER.toString());
        errorMappings.setProperty(FaultException.class.getName(), SoapFaultDefinition.SERVER.toString());
        exceptionResolver.setExceptionMappings(errorMappings);
        exceptionResolver.setOrder(1);
        return exceptionResolver;
    }

    @Bean
    public ServletRegistrationBean messageDispatcherServlet(ApplicationContext applicationContext) {
        MessageDispatcherServlet servlet = new MessageDispatcherServlet();

        servlet.setApplicationContext(applicationContext);
        servlet.setTransformWsdlLocations(true);
        return new ServletRegistrationBean(servlet, "/ws/*");
    }

//    @Override
//    public void addInterceptors(List<EndpointInterceptor> interceptors) {
//        // FIXME add security interceptor
////        interceptors.add(securityInterceptor());
//        super.addInterceptors(interceptors);
//    }
//
//    @Bean UserDetailsService userDetailsService() {
//        return new UserDetailsServiceImpl();
//    }

//    @Bean
//    public SpringSecurityPasswordValidationCallbackHandler securityCallbackHandler(){
//        SpringSecurityPasswordValidationCallbackHandler callbackHandler = new SpringSecurityPasswordValidationCallbackHandler();
//        callbackHandler.setUserDetailsService(userDetailsService());
//
//        return callbackHandler;
//    }
//
//    @Bean
//    public AbstractWsSecurityInterceptor securityInterceptor(){
//            Wss4jSecurityInterceptor securityInterceptor = new Wss4jSecurityInterceptor();
//        securityInterceptor.setValidationActions("UsernameToken");
//        securityInterceptor.setSecurementActions("UsernameToken");
//        securityInterceptor.setSecurementMustUnderstand(true);
//        securityInterceptor.setSecurementPasswordType(WSConstants.PW_TEXT);
//        securityInterceptor.setSecurementUsername("login");
//        securityInterceptor.setSecurementPassword("password");
//        securityInterceptor.setValidationCallbackHandler(securityCallbackHandler());
//        return securityInterceptor;
//    }

//    @Bean
//    public SimplePasswordValidationCallbackHandler securityCallbackHandler(){
//        SimplePasswordValidationCallbackHandler simplePasswordValidationCallbackHandler = new SimplePasswordValidationCallbackHandler();
//        Map userMap = new HashMap();
//        userMap.put("admin", "secret");
//        userMap.put("user", "pass");
//        userMap.put("Ernie","Bert");
//        simplePasswordValidationCallbackHandler.setUsersMap(userMap);
//        return simplePasswordValidationCallbackHandler;
//    }
//
//    @Bean
//    public Wss4jSecurityInterceptor securityInterceptor(){
//        Wss4jSecurityInterceptor wss4jSecurityInterceptor = new Wss4jSecurityInterceptor();
//        wss4jSecurityInterceptor.setValidationActions("UsernameToken");
//        //wss4jSecurityInterceptor.setSecurementActions("UsernameToken");
//        wss4jSecurityInterceptor.setSecurementUsername("Ernie");
//        wss4jSecurityInterceptor.setSecurementPassword("Bert");
//        wss4jSecurityInterceptor.setSecurementPasswordType("PasswordText");
//        wss4jSecurityInterceptor.setValidationCallbackHandler(securityCallbackHandler());
//        return wss4jSecurityInterceptor;
//    }


    @Bean(name = "clients")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema clientsSchema) {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName(Constants.CLIENTS_PORT);
        wsdl11Definition.setLocationUri("/ws");
        wsdl11Definition.setTargetNamespace(Constants.NAMESPACE_URI);
        wsdl11Definition.setSchema(clientsSchema);

        return wsdl11Definition;
    }

    @Bean
    public RestTemplate restTemplate() {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(restUsername, restPassword));

        ResponseErrorHandler handler = new ResponseErrorHandler() {
            @Override
            public boolean hasError(ClientHttpResponse response) throws IOException {
                return !response.getStatusCode().is2xxSuccessful();
            }

            @Override
            public void handleError(ClientHttpResponse response) throws IOException {

                System.err.println(response.getRawStatusCode());
                System.err.println(response.getBody());
            }
        };
        restTemplate.setErrorHandler(handler);
        return restTemplate;
    }

    @Bean
    public XsdSchema clientsSchema() {
        return new SimpleXsdSchema(new ClassPathResource("clients.xsd"));
    }

    @Value("${rest.username}")
    private String restUsername;
    @Value("${rest.password}")
    private String restPassword;
}
