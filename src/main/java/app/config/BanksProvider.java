package app.config;

import com.sun.istack.internal.NotNull;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import app.service.AccountNumberService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class BanksProvider {
    private static String VALID_BANK_REGEX = "^[0-9]{8},(http[s]?:\\/\\/)?([^\\/\\s]+\\/)(.*)$";

//    @Value("${app.banks}}")
//    private String banksListPath;
    private String banksListPath = "banks.csv";

    @Autowired
    private AccountNumberService accountNumberService;
    private Map<String, Bank> banks;

    public BanksProvider() {

    }

    private Bank parse(String line) {
        String singeLine = line.replaceAll("\\s+","");
        if (!singeLine.matches(VALID_BANK_REGEX)) {
            return null;
        }
        String[] entry = singeLine.split(",");
        return new Bank(entry[0], entry[1]);
    }

    private void read() {
        try {
            String path = this.getClass().getClassLoader().getResource(banksListPath).getPath();
            List<String> banksList = Files.readAllLines(
                    Paths.get(path));
            banks = banksList.stream()
                    .skip(1)
                    .map(this::parse)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toMap(Bank::getId, Function.identity()));

        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(banks);
    }


    public @NotNull String getBankAddress(@NotNull @NotBlank String accountNumber) {
        read();
        if (!accountNumberService.isValid(accountNumber)) return null;

        String bankId = accountNumber.substring(2, 10);
        return banks.get(bankId).address;
    }

    private class Bank {
        public Bank(String id, String address) {
            this.id = id;
            this.address = address;
        }

        private String id;
        private String address;

        private String getId() {
            return id;
        }
    }
}