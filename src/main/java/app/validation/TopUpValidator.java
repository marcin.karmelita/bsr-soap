package app.validation;

import com.mk.CashModel;
import com.mk.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.ValidationUtils;
import app.service.AccountNumberService;

@Component
public class TopUpValidator extends Validator<CashModel> {

    @Autowired
    private AccountNumberService accountNumberService;

    @Override
    public void validate(CashModel target) {
        super.validate(target);

        if (target.getAmount() <= 0) {
            bindingResult.rejectValue("amount", "too.min.value", "Value must be greater than 0");
        }

        if (target.getSourceAccount() == null) {
            ValidationUtils.rejectIfEmpty(bindingResult,"account", "null");
        } else {
            if (!target.getSourceAccount().matches("^[0-9]{26}$")) {
                bindingResult.rejectValue("account", "account.regex", "Incorrect account number");
            } else {
                if (!accountNumberService.isValid(target.getSourceAccount())) {
                    bindingResult.rejectValue("account", "account.checksum", "Incorrect checksum");
                }
            }
        }
        if (target.getTitle() == null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(bindingResult, "title", "title.empty");
        }
    }
}
