package app.validation;

import org.springframework.validation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class Validator<T>  {
    protected BindingResult bindingResult;

    public Validator() { }

    public void validate(T target) {
        setup(target);
    }

    private void setup(T target) {
        Map<String,String> map = new HashMap<>();
        bindingResult = new MapBindingResult(map, target.getClass().getName());
    }

    public boolean hasErrors() {
        return bindingResult.hasErrors();

    }

    public List<ObjectError> getAllErrors() {
        return bindingResult.getAllErrors();
    }

    protected void isNull(Object object, String field) {
        if (object == null) {
            ValidationUtils.rejectIfEmpty(bindingResult, field, "null", "Cannot be null");
        }
    }

    protected void matchingRegex(String object, String regex, String field) {
        if (object == null) {
            isNull(object, field);
        } else {
            if (!object.matches(regex)) {
                bindingResult.rejectValue(field, "regex", "Does not match regex ");
            }
        }

    }
}
