package app.validation;

import com.mk.Transaction;
import com.mk.TransferModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class TransferValidator extends Validator<TransferModel> {

    @Autowired
    private TopUpValidator topUpValidator;

    @Override
    public void validate(TransferModel target) {
        super.validate(target);
        topUpValidator.validate(target.getCashModel());
        isNull(target.getDestinationName(), "destination_name");
    }
}
