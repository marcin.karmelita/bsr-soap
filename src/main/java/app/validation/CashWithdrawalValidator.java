package app.validation;

import com.mk.Account;
import com.mk.CashModel;
import com.mk.TransferModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CashWithdrawalValidator extends Validator<CashModel> {

    @Autowired
    private TopUpValidator topUpValidator;

    @Override
    public void validate(CashModel target) {
        super.validate(target);
        topUpValidator.validate(target);
    }

    public void validate(Account account, CashModel target) {
        if (account == null) {
            bindingResult.rejectValue("account", "account", "Account not found.");
        } else {
            boolean negativeBalance = account.getBalance() - target.getAmount() < 0;
            if (negativeBalance) {
                bindingResult.rejectValue("negative", "amount.too.big", "Not enough money on your account.");
            }
        }
    }
}
