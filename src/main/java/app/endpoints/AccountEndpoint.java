package app.endpoints;

import app.faults.FaultException;
import com.mk.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import app.repository.AccountRepository;
import app.service.AccountNumberService;

import java.util.List;

@Endpoint
public class AccountEndpoint {

    @Autowired
    private AccountRepository repository;
    @Autowired
    private AccountNumberService accountNumberService;

    public AccountEndpoint() { }

    @PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = Constants.ENDPOINT_GET_ACCOUNTS)
    @ResponsePayload
    public GetAccountsResponse getAccounts(@RequestPayload GetAccountsRequest request) throws FaultException {
        GetAccountsResponse response = new GetAccountsResponse();
        List<Account> accounts = repository.findByOwnerId(request.getOwnerId());

        response.setAccounts(accounts);
        return response;
    }

    @PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = Constants.ENDPOINT_ADD_ACCOUNT)
    @ResponsePayload
    public AddAccountResponse addAccount(@RequestPayload AddAccountRequest request) throws FaultException {
        AddAccountResponse response = new AddAccountResponse();
        Account account = new Account();
        account.setOwnerId(request.getOwnerId());
        account.setBalance(0);
        account.setAccountNumber(accountNumberService.getNewAccountNumber());
        Account savedAccount = repository.save(account);
        response.setAccount(savedAccount);
        return response;
    }
}