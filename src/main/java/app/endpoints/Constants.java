package app.endpoints;

import javax.validation.constraints.NotNull;

public class Constants {
    public static final String NAMESPACE_URI = "http://mk.com";
    public static final String CLIENTS_PORT = "ClientsPort";
    public static final String ENDPOINT_REGISTER_CLIENT = "registerClientRequest";
    public static final String ENDPOINT_LOGIN_CLIENT = "loginRequest";
    public static final String ENDPOINT_GET_ACCOUNTS = "getAccountsRequest";
    public static final String ENDPOINT_ADD_ACCOUNT = "addAccountRequest";
    public static final String ENDPOINT_GET_TRANSACTIONS = "getTransactionsRequest";
    public static final String ENDPOINT_TRANSFER = "transferRequest";
    public static final String ENDPOINT_CASH_WITHDRAWAL = "cashWithdrawalRequest";
    public static final String ENDPOINT_TOP_UP = "topUpRequest";


    static @NotNull String getRestEndpoint(@NotNull String bankAddress, @NotNull String accountId) {
        return bankAddress + "/accounts/" + accountId + "/history";
    }
}
