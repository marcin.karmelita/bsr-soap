package app.endpoints;

import app.faults.FaultException;
import com.mk.*;
import app.faults.FaultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import app.service.AccountNumberService;
import app.repository.AccountRepository;
import app.repository.ClientRepository;

import static app.service.AccountFactory.*;

@Endpoint
public class ClientEndpoint {

    private ClientRepository repository;
    private AccountRepository accountRepository;
    private AccountNumberService accountNumberService;

    @Autowired
    public ClientEndpoint(ClientRepository repository, AccountRepository accountRepository, AccountNumberService accountNumberService) {
        this.repository = repository;
        this.accountRepository = accountRepository;
        this.accountNumberService = accountNumberService;
    }

    @PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = Constants.ENDPOINT_REGISTER_CLIENT)
    @ResponsePayload
    public RegisterClientResponse registerClient(@RequestPayload RegisterClientRequest request) throws FaultException {
        RegisterClientResponse response = new RegisterClientResponse();
        Login login = request.getLogin();

        if (repository.findByLogin(login.getLogin()) != null) {
            throw FaultFactory.createFaultException(FaultFactory.FaultReason.USER_ALREADY_EXISTS);
        }
        Client client = new Client();
        client.setLogin(login.getLogin());
        client.setPassword(login.getPassword());

        Client savedClient = repository.save(client);

        Account account = create(savedClient, accountNumberService);
        accountRepository.save(account);
        response.setClient(savedClient);
        return response;
    }

    @PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = Constants.ENDPOINT_LOGIN_CLIENT)
    @ResponsePayload
    public LoginResponse loginClient(@RequestPayload LoginRequest request) throws FaultException {
        Login login = request.getLogin();
        Client client = repository.findByLoginAndPassword(login.getLogin(), login.getPassword());
        if (client == null) throw FaultFactory.createException(FaultFactory.FaultReason.USER_DOES_NOT_EXIST);

        LoginResponse response = new LoginResponse();
        response.setClient(client);
        return response;
    }
}
