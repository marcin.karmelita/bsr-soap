package app.endpoints;

import app.faults.FaultException;
import app.service.TransactionBuilder;
import app.validation.CashWithdrawalValidator;
import com.mk.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapBodyException;
import app.models.TransactionModel;
import app.repository.AccountRepository;
import app.config.BanksProvider;
import app.repository.TransactionRepository;
import app.validation.TopUpValidator;
import app.validation.TransferValidator;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Endpoint
public class TransactionEndpoint {

    private static Map<String, String> uriVariables(String address, String accountNumber) {
        Map<String, String> variables = new HashMap<>();
        variables.put("accountId", accountNumber);
        variables.put("host", address);
        return variables;
    }

    @Autowired
    private TransactionRepository repository;
    @Autowired
    private AccountRepository accountRepository;
    @Autowired
    private CashWithdrawalValidator cashWithdrawalValidator;
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private TopUpValidator topUpValidator;
    @Autowired
    private TransferValidator transferValidator;
    @Autowired
    private BanksProvider banksProvider;

    @PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = Constants.ENDPOINT_GET_TRANSACTIONS)
    @ResponsePayload
    public GetTransactionsResponse getTransactions(@RequestPayload GetTransactionsRequest request) throws FaultException {
        GetTransactionsResponse response = new GetTransactionsResponse();
        List<Transaction> transactions = repository.findBySourceAccount(request.getAccountId());
        response.setTransactions(transactions);
        return response;
    }

    @PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = Constants.ENDPOINT_TRANSFER)
    @ResponsePayload
    public TransferResponse transferTransaction(@RequestPayload TransferRequest request) throws FaultException {
        transferValidator.validate(request.getTransferModel());
        if (transferValidator.hasErrors()) throw new SoapBodyException(transferValidator.getAllErrors().toString());
        TransferModel transaction = request.getTransferModel();
        TransactionModel transactionModel = TransactionModel.create(transaction);
        String destinationAccountNumber = transaction.getDestinationAccount();
        String bankAddress = banksProvider.getBankAddress(destinationAccountNumber);


        URI uri = URI.create(Constants.getRestEndpoint(bankAddress, destinationAccountNumber));
        ResponseEntity<Void> restResponse = restTemplate.postForEntity(uri, transactionModel, Void.class);

        System.out.println(restResponse.toString());

//        if (errorModel != null) {
//            throw FaultFactory.createException(errorModel);
//        }
        TransferResponse response = new TransferResponse();
//        Transaction transaction = coordinator.performTransaction(request.getTransaction());
//        response.setTransaction(transaction);
        return response;
    }

    @PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = Constants.ENDPOINT_TOP_UP)
    @ResponsePayload
    @Validated(value = TopUpValidator.class)

    public TopUpResponse topUpTransaction(@RequestPayload TopUpRequest request) throws FaultException {
        topUpValidator.validate(request.getCashModel());
        if (topUpValidator.hasErrors()) throw new SoapBodyException(topUpValidator.getAllErrors().toString());

        CashModel model = request.getCashModel();
        Account account = accountRepository.findByAccountNumber(model.getSourceAccount());
        account.setBalance(account.getBalance() + model.getAmount());

        Transaction transaction = new TransactionBuilder()
                .transactionType(TransactionType.TOP_UP)
                .balance(account.getBalance())
                .cashModel(model)
                .build();

        accountRepository.save(account);
        Transaction responseObject = repository.save(transaction);

        TopUpResponse response = new TopUpResponse();
        response.setTransaction(responseObject);
        return response;
    }

    @PayloadRoot(namespace = Constants.NAMESPACE_URI, localPart = Constants.ENDPOINT_CASH_WITHDRAWAL)
    @ResponsePayload
    public CashWithdrawalResponse withdrawalTransaction(@RequestPayload CashWithdrawalRequest request) throws FaultException {
        cashWithdrawalValidator.validate(request.getCashModel());
        if (cashWithdrawalValidator.hasErrors()) throw new SoapBodyException(cashWithdrawalValidator.getAllErrors().toString());
        CashModel model = request.getCashModel();
        Account account = accountRepository.findByAccountNumber(model.getSourceAccount());
        cashWithdrawalValidator.validate(account, model);
        if (cashWithdrawalValidator.hasErrors()) throw new SoapBodyException(cashWithdrawalValidator.getAllErrors().toString());

        account.setBalance(account.getBalance() - model.getAmount());

        Transaction transaction = new TransactionBuilder()
                .transactionType(TransactionType.CASH_WITHDRAWAL)
                .balance(account.getBalance())
                .cashModel(model)
                .build();

        accountRepository.save(account);
        Transaction responseObject = repository.save(transaction);

        CashWithdrawalResponse response = new CashWithdrawalResponse();
        response.setTransaction(responseObject);
        return response;
    }
}