package app.endpoints;

import com.mk.Account;
import com.mk.Transaction;
import app.faults.FaultFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.soap.SoapFaultException;
import app.repository.AccountRepository;
import app.repository.TransactionRepository;

//@Component
//public class TransactionCoordinator {
//    private TransactionRepository transactionRepository;
//    private AccountRepository accountRepository;
//
//    @Autowired
//    public TransactionCoordinator(TransactionRepository transactionRepository, AccountRepository accountRepository) {
//        this.transactionRepository = transactionRepository;
//        this.accountRepository = accountRepository;
//    }

//    public Transaction performTransaction(Transaction transaction) throws SoapFaultException {
//        Account account = accountRepository.findOne(transaction.getSourceAccount());
//
//        if (account == null) {
//            throw FaultFactory.createException(FaultFactory.FaultReason.SOURCE_ACCOUNT_NOT_EXISTS);
//        }
//
//        Boolean amountGreaterThanZero = transaction.getAmount() > 0;
//        int balanceAfterOperation = account.getBalance() - transaction.getAmount();
//        Boolean balanceAfterTransactionGreaterThanZero = balanceAfterOperation > 0;
//
//        if (!(amountGreaterThanZero && balanceAfterTransactionGreaterThanZero)) {
//            throw FaultFactory.createException(FaultFactory.FaultReason.NOT_ENOUGH_MONEY);
//        }
//
//        account.setBalance(account.getBalance() - transaction.getAmount());
//        accountRepository.save(account);
//        transaction.setBalance(balanceAfterOperation);
//        return transactionRepository.save(transaction);
//    }

//}
