package app.repository;

import com.mk.Client;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface ClientRepository extends MongoRepository<Client, String> {
    Client findByLoginAndPassword(String login, String password);
    Client findByLogin(String login);
}
