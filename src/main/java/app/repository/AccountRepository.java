package app.repository;

import com.mk.Account;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AccountRepository extends MongoRepository<Account, String> {
    public List<Account> findByOwnerId(String ownerId);
    public Account findByAccountNumber(String accountNumber);
}