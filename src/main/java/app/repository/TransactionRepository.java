package app.repository;

import com.mk.Transaction;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Document(collection = "transactions")
public interface TransactionRepository extends MongoRepository<Transaction, String> {
    public List<Transaction> findBySourceAccount(String sourceAccount);
}
